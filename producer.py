from kafka import KafkaProducer


def read_json(filename):
    with open(filename) as f:
        data = f.read()
    return data


def publish_message(producer_instance, topic_name, key, value):
    try:
        key_bytes = bytes(key, encoding='utf-8')
        value_bytes = bytes(value, encoding='utf-8')
        producer_instance.send(topic_name, key=key_bytes, value=value_bytes)
        producer_instance.flush()
        print('Message published successfully.')
    except Exception as ex:
        print('Exception in publishing message')
        print(str(ex))


def connect_kafka_producer():
    _producer = None
    try:
        _producer = KafkaProducer(bootstrap_servers=['localhost:9092'], api_version=(0, 10))
    except Exception as ex:
        print('Exception while connecting Kafka')
        print(str(ex))
    finally:
        return _producer


if __name__ == '__main__':
    kafka_producer = connect_kafka_producer()
    data = read_json("test.json")
    for i in range(1, 11):
        publish_message(kafka_producer, 'json', 'raw', data)

# if __name__ == '__main__':
#     kafka_producer = connect_kafka_producer()
#     for i in range(1, 10):
#         publish_message(kafka_producer, 'raw_recipes', 'raw', "Numer: " + str(i))
