from kafka import KafkaConsumer

if __name__ == '__main__':
    print('Running Consumer..')
    parsed_records = []
    # topic_name = 'raw_recipes'
    topic_name = 'json'

    parsed_topic_name = 'parsed_recipes'

    consumer = KafkaConsumer(topic_name, auto_offset_reset='earliest',
                             bootstrap_servers=['localhost:9092'], api_version=(0, 10), consumer_timeout_ms=1000)
    file1 = open("output.txt", "a")
    for msg in consumer:
        file1.write(msg[6].decode("utf-8"))
    file1.close()

